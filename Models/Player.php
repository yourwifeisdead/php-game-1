<?php

include_once "Interfaces/PlayerInterface.php";

class Player implements PlayerInterface
{

    /**
     * All the types
     */
    CONST TYPES = ['Human', 'Slasher', 'Lolkeker'];

    protected $name = null;
    protected $score = 0;
    protected $health = null;
    protected $damage = null;
    protected $type = null;

    /**
     * Player constructor.
     * @param $name
     * @param $type
     * @param $health
     * @param $damage
     */
    public function __construct($name, $type, $health, $damage)
    {
        $this->name = $name;
        $this->type = $type;
        $this->health = $health;
        $this->damage = $damage;
    }

    /**
     * @param Player $enemy
     */
    public function attack(Player $enemy)
    {
        $enemy_health = $enemy->getHealth();
        $enemy->setHealth($enemy_health - $this->damage);
        echo "\n" . $this->name . " attacked " . $enemy->getName() . " with " . str_repeat("/", $this->damage) . "\n";
    }

    /**
     * @return bool
     */
    public function isDead(): bool
    {
        if ($this->health <= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $toAdd
     */
    public function addTheScore($toAdd)
    {
        $this->score += $toAdd;
    }


//     SETTERS & GETTERS

    public function getName()
    {
        return $this->name;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }

    public function getDamage()
    {
        return $this->damage;
    }

    public function setDamage($damage)
    {
        $this->damage = $damage;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }
}