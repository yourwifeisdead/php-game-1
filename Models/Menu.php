<?php

include_once "Interfaces/MenuInterface.php";

class Menu implements MenuInterface
{
    /**
     * Redirect page to url
     * @param $url
     * @param array|null $params
     */
    public static function redirect($url, array $params = null): void
    {
        $url = 'url=' . $url . '.php';

        if($params !== null) {
            $url .= sprintf('?%s', http_build_query($params));
        }
        header('Refresh:3;' . $url);
    }
}
