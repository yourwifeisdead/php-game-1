<?php

include "Player.php";
include "Enemy.php";

$player = new Player("Rudolf", 10);
$enemy = new Enemy("Alien");
$enemy->setDamage(3);

$enemy->attack($player);