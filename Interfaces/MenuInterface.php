<?php

interface MenuInterface
{
    /**
     * @param $url
     * @param array|null $params
     */
    static function redirect($url, array  $params = null): void;
}