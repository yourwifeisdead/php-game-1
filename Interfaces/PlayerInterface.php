<?php

interface PlayerInterface
{
    /**
     * PlayerInterface constructor.
     * @param $name
     * @param $type
     * @param $health
     * @param $damage
     */
    public function __construct($name, $type, $health, $damage);

    /**
     * @param Player $enemy
     * @return mixed
     */
    public function attack(Player $enemy);

    /**
     * @param $toAdd
     * @return mixed
     */
    public function addTheScore($toAdd);

    /**
     * @return bool
     */
    public function isDead(): bool;

}
