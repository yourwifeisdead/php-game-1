<?php

include_once "Models/Player.php";

class PlayerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /** @test */
    public function IsScoreAdded()
    {
        $player = new Player("Rudolf", 'Lolkeker', 10, 10);
        $this->assertTrue($player->getScore() === 0);
        $player->addTheScore(5);
        $this->assertTrue($player->getScore() === 5);
    }

    /** @test */
    public function AttackMechanics()
    {
        $me = new Player("Rudolf", 'Slasher', 10, 10);
        $enemy = new Player("Alien", 'Human', 15, 5);
        $me->attack($enemy);
        $this->assertTrue($enemy->getHealth() == 5);
    }

    /** @test */
    public function heroIsDead()
    {
        $player = new Player("Bird", 'Lolkeker', 5, 5);
        $player->setHealth(0);
        $this->assertEquals(true, $player->isDead());
    }
}
